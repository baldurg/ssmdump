// imfddump.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

HANDLE hPort;


int _tmain(int argc, _TCHAR* argv[])
{
	unsigned readbytes, commstate;
	DWORD dwCommModemStatus;
	DCB dcb;
	unsigned char buffer[4096];

	hPort = CreateFile(
		argv[1],
		GENERIC_WRITE | GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		//FILE_FLAG_OVERLAPPED,
		NULL
	);

	if (!GetCommState(hPort,&dcb)) {
		printf("Failed to open port\n");
		return 0;
	}
	dcb.BaudRate = 1953; //1953 Baud
	dcb.ByteSize = 8; //8 data bits
	dcb.Parity = EVENPARITY; //even parity
	dcb.StopBits = ONESTOPBIT; //1 stop

	   // Update parameters per Dave Andruczyk and Fastest95PGT, see msefi.com t=23071.
   dcb.fAbortOnError     = false;               // Don't abort
   dcb.fBinary           = true;                // Enable binary mode
   dcb.fDsrSensitivity   = false;               // Ignore DSR line
   dcb.fDtrControl       = DTR_CONTROL_DISABLE; // Disable DTR line
   dcb.fErrorChar        = false;               // Don't replace bad chars
   dcb.fInX              = false;               // Disable XIN
   dcb.fNull             = false;               // Don't drop NULL bytes
   dcb.fOutX             = false;               // Disable XOFF
   dcb.fOutxCtsFlow      = false;               // Don't monitor CTS line
   dcb.fOutxDsrFlow      = false;               // Don't monitor DSR line
   dcb.fParity           = false;               // Disabled
   dcb.fRtsControl       = RTS_CONTROL_DISABLE;
   dcb.wReserved         = false;               // As per msdn.  Beware, not a member of _DCB in 8.0 and later header files.

   	 if (!SetupComm(hPort, 8192, 4096)) {
		 printf("Failed to set port buffers\n");
		 CloseHandle(hPort);
		 return 0;
	 }

	if (!SetCommState(hPort,&dcb)) {
		printf("Failed to set baud\n");
		CloseHandle(hPort);
		return 0;
	}
	readbytes = 0;

	while(1) {
		DWORD bytes;
		SetCommMask (hPort, EV_RXCHAR | EV_ERR); //receive character event
		for(int i = 2; i < argc; i++) {
			char xmitdata[16];
			unsigned short addr = wcstol(argv[i], NULL, 0);
			printf("Dumping address %x\n", addr);
			xmitdata[0] = 0x78;
			xmitdata[1] = addr >> 8;
			xmitdata[2] = addr & 0xFF;
			xmitdata[3] = 0x00;
			//Sleep(2);
			//Sleep(2);
			WriteFile(hPort,xmitdata,4,&bytes,NULL);
			PurgeComm(hPort,PURGE_RXCLEAR | PURGE_TXCLEAR);
			do {
				readbytes = 0;
				while(readbytes < 3) {
					//WaitCommEvent (hPort, &dwCommModemStatus, 0); //wait for character
					ReadFile (hPort, buffer + readbytes, 1, &bytes, 0);
					readbytes += bytes;
				}
				if((buffer[0] == (addr >> 8)) && (buffer[1] == (addr & 0xFF))) printf("Addr %x %x val %x\n", buffer[0], buffer[1], buffer[2]);
			} while(argc == 3 || ((buffer[0] != (addr >> 8)) || (buffer[1] != (addr & 0xFF))));
/*			xmitdata[0] = 0x12;
			xmitdata[1] = 0x00;
			xmitdata[2] = 0x00;
			xmitdata[3] = 0x00;
			WriteFile(hPort,xmitdata,4,&bytes,NULL);
			*/
		}
	}

	return 0;
}

